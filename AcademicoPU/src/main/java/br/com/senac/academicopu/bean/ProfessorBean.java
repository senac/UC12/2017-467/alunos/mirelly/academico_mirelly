
package br.com.senac.academicopu.bean;

import br.com.senac.academicopu.dao.ProfessorDAO;
import br.com.senac.academicopu.model.Professor;
import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "professorBean")
@ViewScoped
public class ProfessorBean implements Serializable{
    
    private Professor professor;
    private ProfessorDAO dao;

    public ProfessorBean() {
        this.professor = new Professor();
        this.dao = new ProfessorDAO();
    }
    
    public void salvar (){
        if (this.professor.getId()==0){
            dao.save(professor);
        }else{
            dao.update(professor);
        }
          
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
    
    
    
    
}
