
package br.com.senac.academicopu.dao;

import br.com.senac.academicopu.model.Aluno;
import java.util.List;
import javax.persistence.Query;


public class AlunoDAO extends DAO<Aluno>{

    
    public AlunoDAO() {
        super(Aluno.class);
    }
    
    public List<Aluno> findByFiltro(String id, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Aluno> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Aluno a where 1=1 ");

        if (id != null && !id.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }
        
         Query query = em.createQuery(sql.toString());
        
        if (id!= null && !id.isEmpty()) {
           query.setParameter("Id", new Long(id));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }


        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }
    
    public static void main(String[] args) {
        
                
        AlunoDAO dao = new AlunoDAO();
        Aluno aluno = new Aluno();
        aluno.setNome("micaella");
        aluno.setCpf("12345678910");
        aluno.setEndereco("rua");
        dao.save(aluno);
        
        
        
        
    }
    
}
