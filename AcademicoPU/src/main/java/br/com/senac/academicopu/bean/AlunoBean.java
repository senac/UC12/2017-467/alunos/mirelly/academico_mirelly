
package br.com.senac.academicopu.bean;

import br.com.senac.academicopu.dao.AlunoDAO;
import br.com.senac.academicopu.dao.JPAUtil;
import br.com.senac.academicopu.model.Aluno;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.persistence.EntityManager;


@Named(value = "alunoBean")
@ViewScoped
public class AlunoBean implements Serializable{
    
    private Aluno aluno;
    private AlunoDAO dao; 
 
    public AlunoBean() {
        this.aluno = new Aluno();
        this.dao = new AlunoDAO();
    }
    
    public void salvar(){
        if(this.aluno.getId()==0){
            dao.save(aluno);
        }else{
            dao.update(aluno);
        }
        
        System.out.println("dasdasd");
        
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }
    
    
    
    
    
}
