
package br.com.senac.academicopu.dao;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAUtil {
    
    private static EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("AcademicoPU");
    
    static EntityManager getEntityManager(){
        try{
            return emf.createEntityManager();
        }catch(Exception e){
            e.printStackTrace();
            throw new RuntimeException("Erro ao acessar banco de dados");
        }
    }
    

}
