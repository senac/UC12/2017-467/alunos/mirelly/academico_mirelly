package br.com.senac.academicopu.bean;

import br.com.senac.academicopu.dao.AlunoDAO;
import br.com.senac.academicopu.model.Aluno;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "pesquisarAlunoBean")
@ViewScoped
public class PesquisarAlunoBean extends Bean {

    private Aluno alunoSelecionado;
    private AlunoDAO dao;
    private List<Aluno> lista;
    
    private String id;
    private String nome;

    public PesquisarAlunoBean() {

    }

    @PostConstruct
    void init() {

        try {
            this.alunoSelecionado = new Aluno();
            this.dao = new AlunoDAO();
            this.lista = dao.findAll();
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    public void pesquisa() {
        try {
            this.lista = this.dao.findByFiltro(id, nome);

        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }
    
    public void salvar(){
        
        if(this.alunoSelecionado.getId() == 0){
            this.dao.save(alunoSelecionado);
            this.addMessageInfo("Salvo com sucesso!");
        }else{
            this.dao.update(alunoSelecionado);
            this.addMessageInfo("Alterado com sucesso!");
        }
        
        this.pesquisa();
        
        
        
    }

    public Aluno getAlunoSelecionado() {
        return alunoSelecionado;
    }

    public void setAlunoSelecionado(Aluno alunoSelecionado) {
        this.alunoSelecionado = alunoSelecionado;
    }

    public List<Aluno> getLista() {
        return lista;
    }

    public void setLista(List<Aluno> lista) {
        this.lista = lista;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

   
}
